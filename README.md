# A network monitoring and NAT traversal appliance

More information on using a "Reverse NAT" on [my blog][blog-post].

## Features

- OpenVPN server called Hermes that should be accessible on the internet.
- OpenVPN client called Hades that can be behind a NAT firewall.  This allows
  one to VPN into the Hermes node, then access the webpages available on Hades.
- Smokeping running on Hades.
- arpwatch running on Hades with logs available on the web server.
- iperf/iperf3 running on Hades.

## Roles

### hades

An OpenVPN client with some network monitoring services:

1. arpwatch with log accessible via http://your-hades.lan/arpwatch.log
2. smokeping with webpages accessible via http://your-hades.lan/smokeping/
3. iperf2/3 services.

### hermes

An OpenVPN server that the hades nodes connect to.  Inherits from base.

### base

Common alpine configurations.

## Bootstrapping the nodes

### Set up Alpine for ansible

First, install Alpine, e.g.:

1. `setup-alpine`.  Make sure to enable ssh daemon and timezone when prompted.
2. Add a user for ansible access with a ssh key and passwordless sudo
3. Run the playbook with the correct role assigned to the host.

#### Minimum commands to bootstrap

```sh
setup-alpine
# Reboot into new install.
apk add python3 sudo
adduser ansible  # Give a bogus long password.
visudo /etc/sudoers.d/ansible  # Enter: `ansible ALL = (ALL) NOPASSWD: ALL`
sudo -u ansible -i
umask 0077
mkdir -p .ssh
wget -O .ssh/authorized_keys https://u.winny.tech/keys/snowcrash  # Install a ssh key.
```

### Set up an easy RSA CA

TODO  See the [blog post][blog-post] for now.

### Configure smokeping

Make a copy with the inventory name in the directory name, then edit its files.

```sh
cp -r smokeping-config.example smokeping-config-ANSIBLE_INVENTORY_NAME
```

### Finally run the playbook

```sh
ansible-playbook -l production main.yml
```

## Upgrading

```sh
ansible-playbook -l production upgrade.yml
```

## License

[Unlicense](./LICENSE)

[blog-post]: https://blog.winny.tech/posts/when-nat-bites-use-reverse-vpn/
